----------------------------------------------------------------------------------
-- Create Date:    10:24:34 10/12/2017 
-- Design Name:    MLP FPGA Implementation
-- Module Name:    Neuron - Behavioral 
-- Project Name: 	 ECSE 682 - Assignment 2
-- Author: Nghia Doan, nghia.doan@mail.mcgill.ca
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
ENTITY MLP IS		
	GENERIC(
		STATE_LENGTH 	: NATURAL := 4;
		DATA_LENGTH_N	: NATURAL := 8;		
		DATA_LENGTH_WB  : NATURAL := 5;		
		FRAC_LENGTH  	: NATURAL := 3
	);
	
	PORT(
		rstn 	: IN STD_LOGIC;  
		clk  	: IN STD_LOGIC;
		wb_rd 	: IN STD_LOGIC;
		wb_in	: IN STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);
		x_rd 	: IN STD_LOGIC;
		x_in 	: IN STD_LOGIC_VECTOR(DATA_LENGTH_N-1 DOWNTO 0);		
		cont  	: IN STD_LOGIC;
		wb_rqst	: OUT STD_LOGIC;
		x_rqst 	: OUT STD_LOGIC;
		state 	: OUT STD_LOGIC_VECTOR(STATE_LENGTH-1 DOWNTO 0);
		y_out	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);	
	
	-- Global type definitions
	SUBTYPE WORD_N  IS STD_LOGIC_VECTOR(DATA_LENGTH_N-1 DOWNTO 0);	
	SUBTYPE WORD_WB IS STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);	
	
	-- BRAM configuration 	
	CONSTANT	ADDR_LENGTH : NATURAL := 12;
	CONSTANT	ADDR_MAX	: NATURAL := 2185;
	CONSTANT	BRAM_TXT 	: STRING(1 to 13):= "Source\WB.txt";
	
	-- MLP configuration	
	--CONSTANT P						: NATURAL := 4;
	CONSTANT NUM_NEURON_UNITS  	: NATURAL := 10;
	CONSTANT NUM_LAYERS			: NATURAL := 3;
	CONSTANT IN_LAYER_SIZE		: NATURAL := 64;
	CONSTANT OUT_LAYER_SIZE		: NATURAL := 10;
	CONSTANT MAX_LAYER_SIZE		: NATURAL := 64;
	SUBTYPE  LAYER_SIZE_TYPE IS NATURAL RANGE 0 TO MAX_LAYER_SIZE;
	TYPE 	 LAYER_DATA_TYPE IS ARRAY(0 TO NUM_LAYERS-1) OF LAYER_SIZE_TYPE;
	TYPE 	 LAYER_ADDR_TYPE IS ARRAY(0 TO NUM_LAYERS-2) OF NATURAL RANGE 0 TO ADDR_MAX-1;
	CONSTANT LAYER_SIZE 		: LAYER_DATA_TYPE := (IN_LAYER_SIZE, 29, OUT_LAYER_SIZE);	
	CONSTANT LAYER_SEG_ADDR 	: LAYER_ADDR_TYPE := (0, 1885);
	TYPE 		X_REG_N_DATA_TYPE IS ARRAY (2*MAX_LAYER_SIZE - 1 DOWNTO 0) OF WORD_N;
			
	-- The number of clock cycles needed for each neuron unit 
	CONSTANT MAX_CNT_LENGTH : NATURAL := 7; -- floor(log_2(MAX_LAYER_SIZE)) + 1
	
	-- Classification
	CONSTANT MIN : INTEGER := -2**(DATA_LENGTH_N-1);
	CONSTANT MAX : INTEGER := 2**(DATA_LENGTH_N-1)-1;
	
END MLP;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
ARCHITECTURE behavioral OF MLP IS
	-- Neuron component
	COMPONENT neuron
		GENERIC(
			DATA_LENGTH_N  : NATURAL;		
			DATA_LENGTH_WB : NATURAL;		
			FRAC_LENGTH  	: NATURAL;
			MAX_CNT_LENGTH : NATURAL
		);
		PORT(
			clk 	: IN STD_LOGIC;
			rstn 	: IN STD_LOGIC;
			n_en	: IN STD_LOGIC;
			M_cnt 	: IN STD_LOGIC_VECTOR(MAX_CNT_LENGTH-1 DOWNTO 0);
			wb		: IN STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);
			x		: IN STD_LOGIC_VECTOR(DATA_LENGTH_N-1 DOWNTO 0);
			y		: OUT STD_LOGIC_VECTOR(DATA_LENGTH_N-1 DOWNTO 0);
			y_rd 	: OUT STD_LOGIC
		);
	END COMPONENT;
	
	-- BRAM component
	COMPONENT BRAM 
	GENERIC(
		DATA_LENGTH_WB 	: NATURAL;
		ADDR_LENGTH 	: NATURAL;
		ADDR_MAX		: NATURAL;
		DATA_FILE 		: STRING
	);
   PORT (		
		clk		: IN STD_LOGIC;			
		wen		: IN STD_LOGIC;
		addr	: IN STD_LOGIC_VECTOR(ADDR_LENGTH-1 DOWNTO 0);
		din		: IN STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);
		dout	: OUT STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0)			
	);
	END COMPONENT;
				
	-- Control signals for neuron components 
	-- (all neurons share the same control signals)
	SIGNAL n_rstn	: STD_LOGIC := '0';
	SIGNAL n_en		: STD_LOGIC := '0';
	SIGNAL M_cnt	: STD_LOGIC_VECTOR(MAX_CNT_LENGTH-1 DOWNTO 0) :=(OTHERS => '0');
	
	-- Data signals for neuron components	
	TYPE N_DATA_TYPE 	IS ARRAY (NUM_NEURON_UNITS-1 DOWNTO 0) OF WORD_N;
	TYPE WB_DATA_TYPE IS ARRAY (NUM_NEURON_UNITS-1 DOWNTO 0) OF WORD_WB;
	TYPE N_Y_RD_TYPE 	IS ARRAY (NUM_NEURON_UNITS-1 DOWNTO 0) OF STD_LOGIC;
	SIGNAL wb 	: WB_DATA_TYPE := (OTHERS => (OTHERS=>'0'));
	SIGNAL x  	: N_DATA_TYPE  := (OTHERS => (OTHERS=>'0'));
	SIGNAL y  	: N_DATA_TYPE;
	SIGNAL y_rd : N_Y_RD_TYPE;
		
	-- Neuron BRAM component
	TYPE BRAM_ADDR_TYPE IS ARRAY (NUM_NEURON_UNITS-1 DOWNTO 0) OF STD_LOGIC_VECTOR(ADDR_LENGTH-1 DOWNTO 0);
	SIGNAL BRAM_wen	: STD_LOGIC	:= '0';	
	SIGNAL BRAM_din	: STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0) := (OTHERS => '0');
	SIGNAL BRAM_addr	: BRAM_ADDR_TYPE := (OTHERS => (OTHERS => '0'));
	
	-- Global signals, and registers				
	SIGNAL x_n_reg : X_REG_N_DATA_TYPE := (OTHERS => (OTHERS => '0'));
				
	SIGNAL n_layer_cnt		: NATURAL RANGE 0 TO NUM_LAYERS := 0;
	SIGNAL n_act_num		: NATURAL RANGE 0 TO NUM_NEURON_UNITS := 0;
	SIGNAL n_last_act_num 	: NATURAL RANGE 0 TO NUM_NEURON_UNITS := 0;
	SIGNAL n_iter_cnt_max 	: NATURAL RANGE 0 TO (INTEGER(MAX_LAYER_SIZE/NUM_NEURON_UNITS)+1) := 0;
	SIGNAL n_iter_cnt 		: NATURAL RANGE 0 TO (INTEGER(MAX_LAYER_SIZE/NUM_NEURON_UNITS)+1) := 0;
	SIGNAL n_in_cnt 		: NATURAL RANGE 0 TO MAX_LAYER_SIZE + 1 := 0;
	SIGNAL n_in_cnt_max 	: NATURAL RANGE 0 TO MAX_LAYER_SIZE + 1 := 0;
	SIGNAL n_layer_seg_addr : NATURAL RANGE 0 TO LAYER_SEG_ADDR(NUM_LAYERS-2):= 0;
	SIGNAL n_batch_seg_addr	: NATURAL RANGE 0 TO NUM_NEURON_UNITS*(MAX_LAYER_SIZE + 1):= 0;  
	SIGNAL n_unit_seg_addr 	: NATURAL RANGE 0 TO MAX_LAYER_SIZE + 1:= 0;
	SIGNAL n_base_seg_addr  : NATURAL RANGE 0 TO ADDR_MAX-1:= 0;
		
	TYPE MLP_STATE IS (INIT, BRAM_INIT, UD_X_IN, INFER_INIT, INFER_CAL, OUTPUT, HALT);
	SIGNAL cState, nState : MLP_STATE := INIT;
	
	SIGNAL wb_in_cnt			: NATURAL RANGE 0 TO ADDR_MAX := 0;
	SIGNAL x_in_cnt				: NATURAL RANGE 0 TO IN_LAYER_SIZE := 0;		
	SIGNAL wb_in_ava			: STD_LOGIC := '0';
	SIGNAL x_in_ava				: STD_LOGIC := '0';	
	SIGNAL x_bf_start			: STD_LOGIC	:= '0';
	SIGNAL infer_init_done		: STD_LOGIC := '0';
	SIGNAL infer_cal_done 		: STD_LOGIC := '0';	
	SIGNAL infer_layer_change	: STD_LOGIC := '0';	
	SIGNAL y_out_done			: STD_LOGIC := '0';
	
	SIGNAL in_reg_seg_addr  : NATURAL RANGE 0 TO MAX_LAYER_SIZE := 0;	
	SIGNAL out_reg_seg_addr : NATURAL RANGE 0 TO MAX_LAYER_SIZE := MAX_LAYER_SIZE;
		
BEGIN
	-- Signal mapping for neuron units
	GEN_NEURON_MAP:
	FOR i IN 0 TO (NUM_NEURON_UNITS-1) GENERATE
		neuron_i : neuron
		GENERIC MAP(
			DATA_LENGTH_N  => DATA_LENGTH_N,		
			DATA_LENGTH_WB => DATA_LENGTH_WB,	
			FRAC_LENGTH    => FRAC_LENGTH,
			MAX_CNT_LENGTH => MAX_CNT_LENGTH
		)
		PORT MAP(
			clk 	=> clk,
			rstn 	=> n_rstn,
			n_en 	=> n_en,
			M_cnt	=> M_cnt,
			wb 		=> wb(i),
			x 		=> x(i),
			y 		=> y(i),
			y_rd 	=> y_rd(i)
		);
	END GENERATE GEN_NEURON_MAP;
	-- end of signal mapping for neuron units
	
	-- Signal mapping for BRAM units
	GEN_BRAM_MAP:
	FOR i IN 0 TO (NUM_NEURON_UNITS-1) GENERATE
		bram_i : BRAM
		GENERIC MAP(
			DATA_LENGTH_WB => DATA_LENGTH_WB,
			ADDR_LENGTH		=> ADDR_LENGTH,			
			ADDR_MAX		=> ADDR_MAX,		
			DATA_FILE 		=> BRAM_TXT
		)
		PORT MAP(		
			clk  => clk,					
			wen  => BRAM_wen,	
			addr => BRAM_addr(i),	
			din  => BRAM_din,	
			dout => wb(i)		
		);
				
	END GENERATE GEN_BRAM_MAP;
	-- end of signal mapping for BRAM units
					
	-- State synchronous
	PROCESS (rstn, clk)
	BEGIN
		IF clk'event AND clk = '1' THEN
			IF rstn = '0' THEN
				cState <= INIT;
			ELSE
				cState <= nState;
			END IF;
		END IF; 
	END PROCESS;
	
	-- Next state update
	PROCESS (cState, wb_in_ava, x_in_ava, infer_init_done, infer_layer_change, 
				infer_cal_done, y_out_done, cont)
	BEGIN
		IF cState = INIT THEN
			state <= STD_LOGIC_VECTOR(TO_UNSIGNED(0,STATE_LENGTH));			
			nState <= UD_X_IN;			
		
		ELSIF cState = BRAM_INIT THEN
			state <= STD_LOGIC_VECTOR(TO_UNSIGNED(1,STATE_LENGTH));			
			IF wb_in_ava = '1' THEN
				nState <= UD_X_IN;
			ELSE
				nState <= BRAM_INIT;
			END IF;	
			
		ELSIF cState = UD_X_IN THEN
			state <= STD_LOGIC_VECTOR(TO_UNSIGNED(2,STATE_LENGTH));
			IF x_in_ava = '1' THEN
				nState <= INFER_INIT;
			ELSE
				nState <= UD_X_IN;
			END IF;	
			
		ELSIF cState = INFER_INIT THEN
			state <= STD_LOGIC_VECTOR(TO_UNSIGNED(3,STATE_LENGTH));
			IF infer_init_done = '1' THEN
				nState <= INFER_CAL;
			ELSE
				nState <= INFER_INIT;
			END IF;
		
		ELSIF cState = INFER_CAL THEN
			state <= STD_LOGIC_VECTOR(TO_UNSIGNED(4,STATE_LENGTH));
			IF infer_cal_done = '1' THEN 
				nState <= OUTPUT;
			ELSIF infer_layer_change = '1' THEN
				nState <= INFER_INIT;
			ELSE
				nState <= INFER_CAL;
			END IF;
					
		ELSIF cState = OUTPUT THEN
			state <= STD_LOGIC_VECTOR(TO_UNSIGNED(5,STATE_LENGTH));
			IF y_out_done = '1' THEN
				nState <= HALT;
			ELSE
				nState <= OUTPUT;
			END IF;
			
		ELSIF cState = HALT THEN
			state <= STD_LOGIC_VECTOR(TO_UNSIGNED(6,STATE_LENGTH));
			IF cont = '1' THEN
				nState <= INIT;
			ELSE
				nState <= HALT;
			END IF;
			
		ELSE
			state <= STD_LOGIC_VECTOR(TO_UNSIGNED(7,STATE_LENGTH));
			nState <= INIT;
			
		END IF;
	END PROCESS;
		
	-- Inference phase
	PROCESS(rstn, clk, cState, x_rd)				
	BEGIN
		IF clk'event AND clk = '1' THEN			
			IF rstn = '0' OR cState = INIT THEN	
				-- control signal for BRAMs
				BRAM_wen <= '0';
				wb_in_cnt <= 0;
				wb_in_ava <= '0';
				wb_rqst <= '0';
							
				-- control signals for x-buffer
				x_rqst <= '0';
				x_in_cnt <= 0;
								
				-- neuron units control signals initilization			
				n_layer_cnt <= 0;							
				n_iter_cnt <= 0;
				n_iter_cnt_max <= 0;		
				n_act_num <= 0;
				n_last_act_num <= 0;				
				n_in_cnt <= 0;
				n_in_cnt_max <= 0;	
				
				-- segment addresses
				n_layer_seg_addr  <= 0;
				n_batch_seg_addr	<= 0;
				n_unit_seg_addr 	<= 0;
				
				-- inference phase status signals				
				infer_init_done <= '0';				
				infer_layer_change <= '0';
				infer_cal_done  <= '0';
				
				-- swap the input and buffered image offset address			
				in_reg_seg_addr <= 0;
				out_reg_seg_addr <= MAX_LAYER_SIZE;
				
			ELSIF cState = BRAM_INIT THEN
				wb_rqst <= '1';				
				IF wb_rd = '1' THEN
					BRAM_wen <= '1';
					IF wb_in_cnt < ADDR_MAX THEN						
						BRAM_din <= wb_in;
						FOR i IN 0 TO (NUM_NEURON_UNITS - 1) LOOP 						
							BRAM_addr(i) <= STD_LOGIC_VECTOR(TO_UNSIGNED(wb_in_cnt,ADDR_LENGTH));						
						END LOOP;
						wb_in_ava <= '0';
						wb_in_cnt <= wb_in_cnt + 1;
					ELSE						
						wb_in_ava <= '1';
						wb_rqst <= '0';	
						BRAM_wen <= '0';
					END IF;
				END IF;
			
			ELSIF cState = INFER_INIT THEN					
				-- calculate the number of required iterations for the curren layer
				-- and the no. of active neurons used in the last iteration
				IF LAYER_SIZE(n_layer_cnt+1) <= NUM_NEURON_UNITS THEN
					n_iter_cnt_max <= 1;
					n_act_num <= LAYER_SIZE(n_layer_cnt+1);
					n_last_act_num <= 0;					
				ELSIF (LAYER_SIZE(n_layer_cnt+1) mod NUM_NEURON_UNITS) = 0 THEN
					n_iter_cnt_max <= INTEGER(LAYER_SIZE(n_layer_cnt+1)/NUM_NEURON_UNITS);
					n_act_num <= NUM_NEURON_UNITS;
					n_last_act_num <= 0;
				ELSE
					n_iter_cnt_max <= INTEGER(LAYER_SIZE(n_layer_cnt+1)/NUM_NEURON_UNITS) + 1;
					n_act_num <= NUM_NEURON_UNITS;
					n_last_act_num <= LAYER_SIZE(n_layer_cnt+1) rem NUM_NEURON_UNITS;
				END IF;	
				
				-- calculate the segment address for each iteration
				n_layer_seg_addr <= LAYER_SEG_ADDR(n_layer_cnt);				
				n_batch_seg_addr <= NUM_NEURON_UNITS*(LAYER_SIZE(n_layer_cnt) + 1);
				n_unit_seg_addr  <= LAYER_SIZE(n_layer_cnt)+1;
				n_base_seg_addr  <= LAYER_SEG_ADDR(n_layer_cnt);
				
				-- the no. of required clock cycles for each neuron unit in the current layer				
				n_in_cnt_max <= LAYER_SIZE(n_layer_cnt) + 1;
				n_in_cnt <= 0;
				n_iter_cnt <= 0;
								
				-- update inference phase status signals 
				infer_init_done <= '1';
				infer_layer_change <= '0';
				infer_cal_done <= '0';			
							
			ELSIF cState = INFER_CAL THEN						
				-- vector-matrix multiplication for one iteration --
				IF infer_cal_done = '0' AND infer_layer_change = '0' THEN					
					IF n_in_cnt = 0 THEN
						-- reset all neurons								
						n_rstn <= '0';
						n_en <= '0';
						M_cnt <= STD_LOGIC_VECTOR(TO_UNSIGNED(LAYER_SIZE(n_layer_cnt),MAX_CNT_LENGTH));	
						-- read the bias value from BRAMS						
						FOR i IN 0 TO (NUM_NEURON_UNITS - 1) LOOP 
							IF i < n_act_num THEN									 
								BRAM_addr(i) <= STD_LOGIC_VECTOR(TO_UNSIGNED(n_base_seg_addr + i*n_unit_seg_addr, ADDR_LENGTH));
							END IF;
						END LOOP;
					ELSE 					
						n_rstn <= '1';
						n_en <= '1';
						FOR i IN 0 TO (NUM_NEURON_UNITS - 1) LOOP 
							IF i < n_act_num THEN
								-- read weights data from brams	
								IF n_in_cnt <= n_in_cnt_max-1 THEN 				
									BRAM_addr(i) <= STD_LOGIC_VECTOR(TO_UNSIGNED(n_base_seg_addr + i*n_unit_seg_addr + n_in_cnt, ADDR_LENGTH));
								END IF;								
								-- read input data
								IF n_in_cnt >= 2 THEN												
									x(i) <= x_n_reg(in_reg_seg_addr + n_in_cnt - 2);
								END IF; 								
							END IF;
						END LOOP;
					END IF;
					
					IF n_in_cnt < n_in_cnt_max THEN
						n_in_cnt <= n_in_cnt + 1;				
					END IF;			 
				
					-- update control signals when 1 iteration is finished
					IF y_rd(0) = '1' AND n_in_cnt = n_in_cnt_max THEN 		
						
						-- store neuron output values
						FOR i IN 0 TO (NUM_NEURON_UNITS - 1) LOOP		
							IF i < n_act_num THEN
								IF n_layer_cnt = NUM_LAYERS - 2 THEN 
									x_n_reg(out_reg_seg_addr + NUM_NEURON_UNITS*n_iter_cnt + i) <= y(i);																	
								ELSE							
									IF y(i)(DATA_LENGTH_N-1) = '0' THEN
										x_n_reg(out_reg_seg_addr + NUM_NEURON_UNITS*n_iter_cnt + i) <= y(i);								
									ELSE
										x_n_reg(out_reg_seg_addr + NUM_NEURON_UNITS*n_iter_cnt + i) <= (OTHERS => '0');								
									END IF;
								END IF;
							END IF;
						END LOOP;

						-- check whether it is the final iteration or not
						-- if not, initialize the control signals for the next iterations
						IF n_iter_cnt = n_iter_cnt_max - 1 THEN 							
							IF n_layer_cnt = NUM_LAYERS - 2 THEN -- last layer condition
								infer_cal_done <= '1';								
							ELSE -- move to the next layer 		
								-- swap the register in-out index								
								in_reg_seg_addr  <= MAX_LAYER_SIZE - in_reg_seg_addr;
								out_reg_seg_addr <= MAX_LAYER_SIZE - out_reg_seg_addr;																
								infer_layer_change <= '1';
								infer_cal_done <= '0';
								n_layer_cnt <= n_layer_cnt + 1;												
							END IF;							
						ELSIF n_iter_cnt = n_iter_cnt_max - 2 THEN 
							-- move to the next iteration 
							n_base_seg_addr <= n_layer_seg_addr + (n_iter_cnt+1)*n_batch_seg_addr;
							n_act_num <= n_last_act_num;
							n_iter_cnt <= n_iter_cnt + 1;
							n_in_cnt <= 0;					
						ELSE
							-- move to the next iteration 
							n_base_seg_addr <= n_layer_seg_addr + (n_iter_cnt+1)*n_batch_seg_addr;
							n_act_num <= NUM_NEURON_UNITS;
							n_iter_cnt <= n_iter_cnt + 1;
							n_in_cnt <= 0;					
						END IF;			
						
					END IF;											
				END IF;												
			END IF;
						
			-- request input data for the test image
			IF cState = UD_X_IN THEN				
				x_rqst <= '1';				
			END IF;
			
			-- read data from the outer source when it's ready
			IF x_rd = '1' THEN					
				x_n_reg(in_reg_seg_addr + x_in_cnt) <= x_in;
				x_in_ava <= '1';				
				-- turn off data request when an image is buffered
				IF x_in_cnt = IN_LAYER_SIZE - 2 THEN 
					x_rqst <= '0'; 
				END IF;
				-- check if the first input image is read
				IF x_in_cnt < IN_LAYER_SIZE-1 THEN
					x_in_cnt <= x_in_cnt + 1;
				ELSE 					
					x_in_cnt <= 0;											
				END IF;				
			END IF;
			
		END IF;
	END PROCESS;
	
	-- Output phase
	PROCESS(rstn, clk, cState)
			VARIABLE y_max : INTEGER RANGE MIN TO MAX := MIN;
			VARIABLE y_idx : NATURAL RANGE 0 TO OUT_LAYER_SIZE-1 := 0;
			VARIABLE y_cnt : NATURAL RANGE 0 TO OUT_LAYER_SIZE-1 := 0;
	BEGIN
		IF clk'event AND clk = '1' THEN
			IF rstn = '0' OR cState = INIT THEN		
				y_max := MIN;
				y_idx := 0;
				y_cnt := 0;
				y_out <= (OTHERS => '1');
				y_out_done <= '0';				
				
			ELSIF cState = OUTPUT THEN		
				IF y_max < TO_INTEGER(SIGNED(x_n_reg(out_reg_seg_addr + y_cnt))) THEN
					y_max := TO_INTEGER(SIGNED(x_n_reg(out_reg_seg_addr + y_cnt)));
					y_idx := y_cnt ;
				END IF;
				
				IF y_cnt < OUT_LAYER_SIZE-1 THEN
					y_cnt := y_cnt + 1;
				ELSE
					y_out <= STD_LOGIC_VECTOR(TO_UNSIGNED(y_idx,4));
					y_out_done <= '1';			
				END IF;		
				
			END IF;
		END IF;
	END PROCESS;
	
END behavioral;
----------------------------------------------------------------------------------















