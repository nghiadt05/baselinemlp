----------------------------------------------------------------------------------
-- Create Date:    12:54:42 10/10/2017 
-- Design Name:    MLP FPGA Implementation
-- Module Name:    BRAM - Behavioral 
-- Project Name: 	 ECSE 682 - Assignment 2
-- Author: Nghia Doan, nghia.doan@mail.mcgill.ca
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE STD.TEXTIO.ALL;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
ENTITY BRAM IS
	GENERIC(
		DATA_LENGTH_WB : NATURAL := 6;
		ADDR_LENGTH : NATURAL := 12;
		ADDR_MAX		: NATURAL := 2185;
		DATA_FILE 	: STRING(1 to 13):= "Source/WB.txt"
	);
   PORT (		
		clk	: IN STD_LOGIC;			
		wen	: IN STD_LOGIC;
		addr	: IN STD_LOGIC_VECTOR(ADDR_LENGTH-1 DOWNTO 0);
		din	: IN STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);
		dout	: OUT STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0)			
	);
END BRAM;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
ARCHITECTURE behavior OF BRAM IS

	SUBTYPE WORD IS STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);
	TYPE BRAM_TYPE IS ARRAY (0 TO ADDR_MAX-1) OF WORD;	
	
	-- Initialize BRAM data from file
	IMPURE FUNCTION InitBRAM(DataFile : IN STRING) RETURN BRAM_TYPE IS
		FILE BRAMFile : TEXT OPEN READ_MODE IS DataFile;
		VARIABLE line : LINE;
		VARIABLE inData : BIT_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);
		VARIABLE BRAM_DATA : BRAM_TYPE; 
	BEGIN
		FOR i IN 0 TO ADDR_MAX-1 LOOP			
			IF (NOT ENDFILE(BRAMFile)) THEN
				READLINE(BRAMFile,line);
				READ(line, inData);
				BRAM_DATA(i) := TO_STDLOGICVECTOR(inData);			
			END IF;
		END LOOP;
		RETURN BRAM_DATA;
	END FUNCTION;	
	SIGNAL BRAM_DATA : BRAM_TYPE := InitBRAM(DATA_FILE);	
--	SIGNAL BRAM_DATA : BRAM_TYPE := (OTHERS => (OTHERS => '1'));
BEGIN
	
	PROCESS (clk)
	BEGIN
		-- PORT A
		IF clk'event AND clk = '1' THEN				
			IF wen = '1' THEN
				BRAM_DATA(CONV_INTEGER(addr)) <= din;
			END IF;
			dout <= BRAM_DATA(CONV_INTEGER(addr));					
		END IF;			
	END PROCESS;
	
END behavior;
----------------------------------------------------------------------------------

