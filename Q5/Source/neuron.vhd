----------------------------------------------------------------------------------
-- Create Date:    09:26:28 10/11/2017 
-- Design Name:    MLP FPGA Implementation
-- Module Name:    Neuron - Behavioral 
-- Project Name: 	 ECSE 682 - Assignment 2
-- Author: Nghia Doan, nghia.doan@mail.mcgill.ca
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
ENTITY neuron IS
	GENERIC(
		DATA_LENGTH_N  	: NATURAL := 8;		
		DATA_LENGTH_WB 	: NATURAL := 5;		
		FRAC_LENGTH			: NATURAL := 3;
		MAX_CNT_LENGTH 	: NATURAL := 7
	);
	PORT(
		clk 	: IN STD_LOGIC;
		rstn 	: IN STD_LOGIC;
		n_en	: IN STD_LOGIC;
		M_cnt	: IN STD_LOGIC_VECTOR(MAX_CNT_LENGTH-1 DOWNTO 0);
		wb		: IN STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);
		x		: IN STD_LOGIC_VECTOR(DATA_LENGTH_N-1 DOWNTO 0);
		y		: OUT STD_LOGIC_VECTOR(DATA_LENGTH_N-1 DOWNTO 0);
		y_rd 	: OUT STD_LOGIC 
	);
	
	CONSTANT MIN : SIGNED(DATA_LENGTH_N-1 DOWNTO 0) := (DATA_LENGTH_N-1 => '1', OTHERS => '0');
	CONSTANT MAX : SIGNED(DATA_LENGTH_N-1 DOWNTO 0) := (DATA_LENGTH_N-1 => '0', OTHERS => '1');
END neuron;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF neuron IS	
	SIGNAL neuron_cnt : NATURAL RANGE 0 TO 2**MAX_CNT_LENGTH - 1 := 0;		
	SIGNAL tmp_y : SIGNED(DATA_LENGTH_N-1 DOWNTO 0):= (OTHERS => '0');		
	SIGNAL tmp_y_rd : STD_LOGIC := '0';
BEGIN	
	-- Vector-matrix multiplication
	vector_matrix_mul: PROCESS(clk, rstn, n_en)		
		VARIABLE temp : SIGNED(DATA_LENGTH_N+DATA_LENGTH_WB-1 DOWNTO 0):= (OTHERS => '0');		
	BEGIN
		IF clk'event AND clk = '1' THEN
			IF rstn = '0' THEN
				neuron_cnt <= 0;	
				tmp_y <= (OTHERS => '0');	
				tmp_y_rd <= '0';			
				temp := (OTHERS=>'0');
				 
			ELSIF n_en = '1' THEN				
				-- the bias value comes first at the first clock cycle
				IF neuron_cnt = 0 THEN	
					FOR i IN DATA_LENGTH_WB TO DATA_LENGTH_N+DATA_LENGTH_WB-1 LOOP
						temp(i) := wb(DATA_LENGTH_WB-1);
					END LOOP;
					temp(DATA_LENGTH_WB+FRAC_LENGTH-1 DOWNTO FRAC_LENGTH) := SIGNED(wb); 					
				-- the weights and the correspond x values come after the first clock cycle
				ELSIF neuron_cnt <= UNSIGNED(M_cnt) AND tmp_y_rd = '0' THEN	
					temp := SIGNED(wb)*SIGNED(x) + temp;					
					-- need overflow check !!!!
					IF temp(DATA_LENGTH_N+DATA_LENGTH_WB-1 DOWNTO FRAC_LENGTH) > MAX THEN
						tmp_y <= (DATA_LENGTH_N-1 => '0', OTHERS => '1');
					ELSIF temp(DATA_LENGTH_N+DATA_LENGTH_WB-1 DOWNTO FRAC_LENGTH) < MIN THEN
						tmp_y <= (DATA_LENGTH_N-1 => '1', OTHERS => '0');
					ELSE
						tmp_y <= temp(DATA_LENGTH_N+FRAC_LENGTH-1 DOWNTO FRAC_LENGTH);	
					END IF;									
				END IF;	

				-- internal neuron counter
				IF neuron_cnt < UNSIGNED(M_cnt) THEN
					neuron_cnt <= neuron_cnt + 1;
					tmp_y_rd <= '0';
				ELSE  
					tmp_y_rd <= '1';					
				END IF;					
				
			END IF; 
		END IF;
	END PROCESS;
	
	-- output signal assignments		
	y <= STD_LOGIC_VECTOR(tmp_y);
	y_rd <= tmp_y_rd;
	
END Behavioral;
----------------------------------------------------------------------------------

