--------------------------------------------------------------------------------
-- Create Date:   13:29:23 10/12/2017
-- Design Name:    MLP FPGA Implementation
-- Module Name:    MLP - Testbench
-- Project Name: 	 ECSE 682 - Assignment 2
-- Author: Nghia Doan, nghia.doan@mail.mcgill.ca
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE STD.TEXTIO.ALL;
--------------------------------------------------------------------------------
 
--------------------------------------------------------------------------------
ENTITY MLP_TB IS
	CONSTANT STATE_LENGTH 	: NATURAL := 4;
	CONSTANT DATA_LENGTH_N  : NATURAL := 8;		
	CONSTANT DATA_LENGTH_WB : NATURAL := 5;		
	CONSTANT FRAC_LENGTH  	: NATURAL := 3;
	CONSTANT TEST_SIZE	 	: NATURAL := 10000;
END MLP_TB;
--------------------------------------------------------------------------------
 
--------------------------------------------------------------------------------
ARCHITECTURE behavior OF MLP_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT) 
    COMPONENT MLP	 
	 GENERIC(
			STATE_LENGTH	: NATURAL;
			DATA_LENGTH_N	: NATURAL;		
			DATA_LENGTH_WB : NATURAL;		
			FRAC_LENGTH  	: NATURAL
			);
    PORT(
         rstn 	: IN STD_LOGIC;
			clk  	: IN STD_LOGIC;
			wb_rd : IN STD_LOGIC;
			wb_in	: IN STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0);
			x_rd 	: IN STD_LOGIC;
			x_in 	: IN STD_LOGIC_VECTOR(DATA_LENGTH_N - 1 DOWNTO 0);
			cont  : IN STD_LOGIC;
			wb_rqst : OUT STD_LOGIC;
			x_rqst  : OUT STD_LOGIC;
			state : OUT STD_LOGIC_VECTOR(STATE_LENGTH - 1 DOWNTO 0);
			y_out	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
        );
    END COMPONENT;    
	 
   --Inputs
   SIGNAL rstn	: STD_LOGIC := '0';
   SIGNAL clk	: STD_LOGIC := '0';
	SIGNAL wb_rd : STD_LOGIC := '0';
	SIGNAL wb_in : STD_LOGIC_VECTOR(DATA_LENGTH_WB-1 DOWNTO 0) := (OTHERS => '0');
	SIGNAL x_rd : STD_LOGIC := '0';
   SIGNAL x_in	: STD_LOGIC_VECTOR(DATA_LENGTH_N-1 DOWNTO 0) := (OTHERS => '0');
	SIGNAL cont : STD_LOGIC := '0';   

 	--Outputs
	SIGNAL wb_rqst	: STD_LOGIC;
	SIGNAL x_rqst	: STD_LOGIC;
   SIGNAL state	: STD_LOGIC_VECTOR(STATE_LENGTH-1 DOWNTO 0);
	SIGNAL y_out	: STD_LOGIC_VECTOR(3 DOWNTO 0);	
	SIGNAL y_test  : STD_LOGIC_VECTOR(3 DOWNTO 0);
		
	-- Error and test count numbers
	SIGNAL error_cnt : NATURAL := 0;
	SIGNAL test_cnt  : NATURAL := 1;

   -- Clock period definitions
   CONSTANT clk_period : TIME := 10 NS;
	
	-- constants
	CONSTANT INIT 		: NATURAL := 0;
	CONSTANT UD_X_IN 	: NATURAL := 2;
	CONSTANT OUTPUT 	: NATURAL := 5;
	CONSTANT HALT 		: NATURAL := 6;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MLP 
	GENERIC MAP(
		STATE_LENGTH 	=> STATE_LENGTH,
		DATA_LENGTH_N	=> DATA_LENGTH_N,		
		DATA_LENGTH_WB => DATA_LENGTH_WB,		
		FRAC_LENGTH  	=> FRAC_LENGTH
	)
	PORT MAP (
		 rstn => rstn,
		 clk => clk,
		 wb_rd => wb_rd,
		 wb_in => wb_in,
		 x_rd => x_rd,
		 x_in => x_in,        
		 cont => cont,
		 wb_rqst => wb_rqst,
		 x_rqst => x_rqst,
		 state => state,
		 y_out => y_out
	  );

   -- Clock PROCESS definitions
   clk_PROCESS :PROCESS
   BEGIN
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   END PROCESS;
 
   -- Stimulus PROCESS
   stim_proc: PROCESS
   BEGIN		
      -- hold reset state for 100 ns.
		rstn <= '0';
      wait for 100 ns;	
		rstn <= '1';      
      wait;
   END PROCESS;
		
	-- write test image if required by the MLP module
	PROCESS (clk, x_rqst)
		FILE TestFile 	 : TEXT OPEN READ_MODE IS "Source\X.txt";
		VARIABLE in_cnt : NATURAL RANGE 0 TO 64 := 0;
		VARIABLE lineX : LINE;
		VARIABLE inXData : BIT_VECTOR(DATA_LENGTH_N-1 DOWNTO 0);
	BEGIN
		IF clk'event AND clk = '1' THEN			
			IF TO_INTEGER(UNSIGNED(state)) = INIT THEN
				in_cnt := 0;
				x_rd <= '0'; 
			ELSIF x_rqst = '1' THEN							
				IF (NOT ENDFILE(TestFile)) THEN
					READLINE(TestFile,lineX);
					READ(lineX, inXData);
					x_in <= TO_STDLOGICVECTOR(inXData);
				END IF;
				x_rd <= '1'; 								
			ELSE
				x_rd <= '0'; 
			END IF;		
		END IF;
	END PROCESS;	
	
	-- test the classified image given by the MLP module
	PROCESS(state, clk)				
		FILE ResultFile : TEXT OPEN READ_MODE IS "Source\Y.txt";						
		VARIABLE lineY : LINE;				
		VARIABLE inYData : BIT_VECTOR(3 DOWNTO 0);		
		VARIABLE isTested : STD_LOGIC := '0';	
	BEGIN
		IF clk'event AND clk = '1' THEN
			IF TO_INTEGER(UNSIGNED(state)) = INIT THEN				
				cont <= '0';				
				isTested := '0';	
				y_test <= (OTHERS => '1');
			ELSIF TO_INTEGER(UNSIGNED(state)) = HALT THEN
				IF isTested = '0' THEN
					-- read labels and compare the results
					IF (NOT ENDFILE(ResultFile)) AND isTested = '0' THEN									
						READLINE(ResultFile,lineY);
						READ(lineY, inYData);
						y_test <= TO_STDLOGICVECTOR(inYData);
						IF TO_STDLOGICVECTOR(inYData) /= y_out THEN
							error_cnt <= error_cnt + 1;
						END IF;	
					
						IF test_cnt < TEST_SIZE THEN
							cont <= '1';								
							test_cnt <= test_cnt + 1;
						ELSE
							cont <= '0';								
						END IF;
						
						isTested := '1';		
						
						ASSERT test_cnt < TEST_SIZE 
						REPORT "TEST DONE !!!, error = " & integer'image(error_cnt) & "/" & integer'image(test_cnt)
						SEVERITY FAILURE; 
						
					END IF;
				END IF;					
			END IF;
		END IF;
	END PROCESS;
	
END;
--------------------------------------------------------------------------------
