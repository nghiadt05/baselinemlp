#!/usr/bin/env python

#Read trained networks' configurations
#Author: Nghia Doan, nghia.doan@mail.mcgill.ca
#Date  : 2017, Oct 5th, 11:14 AM

import scipy.io
import numpy as np

# read result file 
netConfig = './results/nghia.npz'
npzFile = np.load(netConfig)

# define the index of the chosen design
deIdx = 11
paWs  = npzFile['pareto_weights']
deWs = paWs[deIdx] # weight arrays of the chosen design
print ('Chosen Paret-Design Index: ', deIdx)

# overall infor of the network 
LayerDepth = int(len(deWs)/2) + 1
LayerConfig = []
print ('Number of layers:', LayerDepth)
for i in range(0,LayerDepth-1):
	LayerConfig.append(len(deWs[i*2]))
LayerConfig.append(len(deWs[len(deWs)-1]))
print ('Layer configurations: ', LayerConfig)

# extract weights to matfile
print ('Extracting weights and bias to mat files ...')
for i in range(0, LayerDepth-1):
	wFile = './matFiles/weights_layer_'+str(i)+'.mat'
	bFile = './matFiles/bias_layer_'+str(i)+'.mat'
	w = deWs[2*i]
	b = deWs[2*i+1]
	scipy.io.savemat(wFile,mdict={'w':w})
	scipy.io.savemat(bFile,mdict={'b':b})
print ('... done, check the files in ./matFiles')
	
	
