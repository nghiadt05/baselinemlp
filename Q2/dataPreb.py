#!/usr/bin/env python

#Data preparation for training the reduced MNIST datasets
#Author: Nghia Doan, nghia.doan@mail.mcgill.ca
#Date  : 2017, Oct 5th, 11:14 AM

import numpy as np
import random

print('Data preparation for the reduced MNIST datasets')

# data sizes
DATA_SIZE = 70000
TRAIN_SIZE = 50000
VAL_SIZE = 10000
#TEST_SIZE = 10000 #  remaining data

# read data from files
X = np.load('./Reduced-MNIST_X.npy')
Y = np.load('./Reduced-MNIST_Y.npy')
tmpX = X[:]
tmpY = Y[:]

# create a shuffled list of data
mixedList = list(range(DATA_SIZE))
random.shuffle(mixedList)
random.shuffle(mixedList)
for i in range(0, DATA_SIZE):
	idx = mixedList[i]
	tmpX[i] = X[idx]
	tmpY[i] = Y[idx]
X = tmpX[:]
Y = tmpY[:]

# assign the shuffled data to appopriate sets
X_TRAIN = X[:TRAIN_SIZE]
Y_TRAIN = Y[:TRAIN_SIZE]

X_VAL	= X[TRAIN_SIZE:(TRAIN_SIZE+VAL_SIZE)]
Y_VAL	= Y[TRAIN_SIZE:(TRAIN_SIZE+VAL_SIZE)]

X_TEST 	= X[(TRAIN_SIZE+VAL_SIZE):]
Y_TEST  = Y[(TRAIN_SIZE+VAL_SIZE):]

# save the shuffled data to appropriate sets
#np.save('./Reduced-MNIST_X_TRAIN.npy', X_TRAIN);
#np.save('./Reduced-MNIST_Y_TRAIN.npy', Y_TRAIN);

#np.save('./Reduced-MNIST_X_VAL.npy', X_VAL);
#np.save('./Reduced-MNIST_Y_VAL.npy', Y_VAL);

#np.save('./Reduced-MNIST_X_TEST.npy', X_TEST);
#np.save('./Reduced-MNIST_Y_TEST.npy', Y_TEST);

print('Done ! You can now use the created data to train your MLP.')
