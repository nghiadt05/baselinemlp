%%
clc; close all; clear all;

%% Read result files 
fid = fopen('./nghia_relu.txt','r');
line = 1;
isParResults = 0;
isAllResults = 0;
PaResults = []; PaLayers = []; PaLayerIdx = 1;
AllResults = []; 
while ~feof(fid)
    tline = fgetl(fid);
    % read pareto results
    if (~isempty(tline) && ~strcmp(tline,'All results:') && isParResults == 1) 
        %disp(tline);
        [ID, error, cost, layer] = extractResult(tline);     
        PaResults = [PaResults; [ID, error, cost]];       
        PaLayers{PaLayerIdx} = layer;
        PaLayerIdx = PaLayerIdx + 1;
    end
    % read all results
    if (~isempty(tline) && isAllResults == 1) 
        %disp(tline);
        [ID, error, cost, layer] = extractResult(tline);     
        AllResults = [AllResults; [ID, error, cost]];
    end
    
    if strcmp(tline,'Pareto-optimal results:')
        isParResults = 1;
        isAllResults = 0;
    elseif strcmp(tline,'All results:')
        isParResults = 0;
        isAllResults = 1;        
    end  
    line = line + 1;
end

%% Plot the exploration
figure('name', 'MNIST with OPAL');
hold all;
grid on;
grid minor;
title('OPAL with Reduced MNIST dataset')
xlabel('Prediction Error');
ylabel('Estimated HW Cost');
plot( AllResults(:,2),AllResults(:,3),'.',...
      'MarkerSize',15,...
      'MarkerFaceColor','b',...
      'MarkerEdgeColor','b');
plot( PaResults(:,2),PaResults(:,3),'.',...
      'MarkerSize',15,...
      'MarkerFaceColor','r',...
      'MarkerEdgeColor','r');
legend('All results','Pareto-optimal results');


