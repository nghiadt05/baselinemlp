function [ ID, error, cost, layer] = extractResult(tline)
    IDStr = tline(1:find(tline=='[')-1);
    ID = textscan(IDStr,'%d %s','Delimiter',':');
    ID = double(ID{1});
    layerStr = tline(find(tline=='[')+1:find(tline==']')-1);
    layer = textscan(layerStr, '%d', 'Delimiter', ',');
    otherStr = tline(find(tline==']')+2:end);
    others = textscan(otherStr,'%s', 'Delimiter', ',');
    error = textscan(char(others{1}(2)),'%s %f', 'Delimiter', ':' );
    error = error{2};
    cost = textscan(char(others{1}(3)),'%s %f', 'Delimiter', ':' );
    cost = cost{2};   
end

