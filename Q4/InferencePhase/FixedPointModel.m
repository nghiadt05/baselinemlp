%%
clc; close all; clear all;

%% FNN config
fc_activation =	'relu'; 
out_activation = 'linear';

%% This program uses a 3rd-party library (npy-matlab) to read npy files
addpath(genpath('../npy-matlab-master/')); 

%% Read the test data needed for the inference phase
X_TEST = readNPY('./Reduced-MNIST_X_TEST.npy'); 
Y_TEST = readNPY('./Reduced-MNIST_Y_TEST.npy'); 

DataSizeX = size(X_TEST);
DataSizeY = size(Y_TEST);

tmpX_TEST = X_TEST;
tmpY_TEST = Y_TEST;
clear X_TEST Y_TEST;

for i = 1:DataSizeX(1)
    tmpX = [];
    for j = 1:DataSizeX(2)
        for k = 1:DataSizeX(3)
           tmpX((j-1)*DataSizeX(3)+k) =  tmpX_TEST(i,j,k);
        end
    end
    
    tmpY = [];
    for j = 1:DataSizeY(2)
        tmpY(j) = tmpY_TEST(i,j);
    end
    
    X_TEST{i} = tmpX;
    Y_TEST{i} = tmpY;
end
clear tmpX tmpY tmpX_TEST tmpY_TEST

%% Read the weight and bias values of the FNN
addpath(genpath('./matFiles'));
Config = 'Config_12';% Config_x where x = {4,8,10,12} only
% Read network size and calculate the amount of memory words for storing
% weights and bias
if strcmp(fc_activation, 'relu')
    flayer = fopen(sprintf('./matFiles/%s/%s.txt',Config,Config),'r');
    flayer = textscan(flayer,'%s','Delimiter','[],');    
    L = [];
    for i = 1: length(flayer{1})
        if ~strcmp(flayer{1}(i),'')
            L = [L, str2num(char(flayer{1}(i)))];
        end
    end   
end

WB_NumWords = 0;
for i=1:length(L)-1
    WB_NumWords = WB_NumWords + (L(i)+1)*L(i+1);
end

MAX_W = -99999999999999;
MAX_B = -99999999999999;
for i = 1:length(L)-1
    if strcmp(fc_activation, 'relu')
        tmpW = load(sprintf('./matFiles/%s/weights_layer_%d.mat',Config,i-1));
        tmpB = load(sprintf('./matFiles/%s/bias_layer_%d.mat',Config,i-1));
    end
    W{i} = tmpW.w;
    B{i} = tmpB.b;  
    
    % update the max/min float numbers
    if max(max(abs(W{i}))) > MAX_W 
        MAX_W = max(max(abs(W{i})));
    end  
    if max(max(abs(B{i}))) > MAX_B 
        MAX_B = max(max(abs(B{i})));
    end  
    MAX_X(i) = -9999999999;
end
MAX_X(length(L)) = -9999999999;
clear tmpW tmpB flayer

%% Inference phase in floating point format
ECntFlt = 0;
for i=1:DataSizeX(1)
    % take a new test image
    X{1} = X_TEST{i};
    
    % update the max/min float numbers
    if max(abs(X{1})) > MAX_X(1) 
        MAX_X(1) = max(abs(X{1}));
    end    
    
    % inference phase of the FNN
    for j = 1: length(L)-1
        % vector-matrix multiplication added bias values
        X{j+1} = X{j}*W{j} + B{j};
        % update the max/min float numbers
        if max(abs(X{j+1})) > MAX_X(j+1) 
            MAX_X(j+1) = max(abs(X{j+1}));
        end     
        % activation functions
        if j < length(L)-1 % hidden layers
            if strcmp(fc_activation,'relu')
                X{j+1} = ReLU(X{j+1});
            end
        else % output layers (softmax or linear (do nothing))
            if strcmp(out_activation,'softmax') 
                tmpExp = exp(X{j+1});
                sumExp = sum(tmpExp);
                X{j+1} = X{j+1}./sumExp;
            end
        end        
    end   
    % classification    
    [~, idX] = max(X{length(L)});
    [~, idY] = max(Y_TEST{i});
    if idX ~= idY
        ECntFlt = ECntFlt + 1;
    end
end
ErrorFlt = ECntFlt/DataSizeX(1);

%% Fixed-point model implementation
signed = 1;
m = 3;
n1 = 4;
n2 = 1;
wordLength_N = n1 + m + 1;
wordLength_WB = n2 + m + 1;
LUT_Size = 1000;
fixMAX_WB = 2^(n2 + m) - 1;
fixMIN_WB = -2^(n2 + m);
fixMAX = 2^(n1 + m) - 1;
fixMIN = - 2^(n1 + m);

% convert the weight and bias values to fixed-point numbers
for i = 1:length(L)-1        
    fixW{i} = constrain( rounding(W{i}*2^m,'round'), fixMAX_WB, fixMIN_WB);    
    fixB{i} = constrain( rounding(B{i}*2^m,'round'), fixMAX_WB, fixMIN_WB);
end

% Inference phase in fixed point format
ECntFix = 0;
for i=1:DataSizeX(1)
    % take a new test image
    fixX{1} = constrain(rounding((X_TEST{i}*2^m),'round'),fixMAX, fixMIN);   
    % inference phase of the FNN
    for j = 1: length(L)-1             
        fixX{j+1} = constrain(rounding(fixX{j}*fixW{j}/2^m,'floor')+fixB{j},fixMAX, fixMIN);
        % activation functions
        if j < length(L)-1 % hidden layers (tanh or ReLU)
            if strcmp(fc_activation,'tanh')                 
                fixX{j+1} = tanhLUT(tLUT,fixX{j+1});
            elseif strcmp(fc_activation,'relu')
                fixX{j+1} = ReLU(fixX{j+1});  
            end
        end       
    end
    % classification 
    [~, idX] = max(fixX{length(L)});
    [~, idY] = max(Y_TEST{i});
    if idX ~= idY
        ECntFix = ECntFix + 1;
    end
end
ErrorFix = ECntFix/DataSizeX(1);

%% Print out summary configuration
fprintf('%s\n',Config);
fprintf('Network size:'); L
fprintf('Num of memory words: %d\n',WB_NumWords);
fprintf('Fixed-point format: Q%d.%d\n',n1,m);
fprintf('Floating-point error rate: %0.5f\n',ErrorFlt);
fprintf('Fixed-point error rate: %0.5f\n',ErrorFix);

%% Write data to text files
% test images
if ~exist('./X.txt','file')
    fid_X = fopen('X.txt','w');   
    for i = 1:length(X_TEST)
        tmp = constrain(rounding(X_TEST{i}*2^m,'round'), fixMAX, fixMIN);
        tmpXq = fi(tmp,signed,wordLength_N,0);
        tmpBin = tmpXq.bin;
        tmpBin = textscan(tmpBin,'%s');        
        for j=1:length(tmpBin{1})
            fprintf(fid_X,'%s\n',char(tmpBin{1}(j)));
        end             
    end  
    fclose(fid_X);
end

% output class
if ~exist('./Y.txt','file')
    fid_Y = fopen('Y.txt','w');   
    for i = 1:length(X_TEST)
        [~, idY] = max(Y_TEST{i});
        tmpYq = fi(idY-1,0,4,0);
        tmpBin = tmpYq.bin;
        tmpBin = textscan(tmpBin,'%s');        
        for j=1:length(tmpBin{1})
            fprintf(fid_Y,'%s\n',char(tmpBin{1}(j)));
        end             
    end  
    fclose(fid_Y);
end

% weights and bias
if ~exist('./WB.txt','file')
    WB_ALL = [];
    fid_WB = fopen('WB.txt','w');   
    for i = 1:length(W)       
        curW = [B{i};W{i}];
        sizeW = size(curW);
        for j = 1:sizeW(2)            
            tmp = constrain(rounding(curW(:,j)*2^m,'round'), fixMAX_WB, fixMIN_WB);
            Wq = fi(tmp,signed,wordLength_WB,0);
            Wq = Wq.bin;
            WB_ALL = [WB_ALL;Wq];
            sizeWq = size(Wq);
            for k = 1:sizeWq(1)
                fprintf(fid_WB,'%s\n',Wq(k,:));
            end
        end
    end
    fclose(fid_WB);
end
copyfile ./*.txt ../../Q5/Source/;
