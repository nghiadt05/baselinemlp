function Y = ReLU(X)
    Y = 0.5*(X + abs(X));  
end