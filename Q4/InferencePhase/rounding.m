function Y = rounding( X, opt )
%ROUNDING Summary of this function goes here
%   Detailed explanation goes here
 if strcmp(opt,'round')
    X = round(X);
 elseif strcmp(opt,'floor')
    posIdx = find(X>0);
    negIdx = find(X<0);
    X(posIdx) = floor(X(posIdx));
    X(negIdx) = floor(X(negIdx));
 end
 Y = X;
end

