function Y = constrain(X, fixMAX, fixMIN)
       ovfIdx = find(X > fixMAX);
    if ~isempty(ovfIdx)
        X(ovfIdx) = fixMAX;
    end
    ovfIdx = find(X < fixMIN);
    if ~isempty(ovfIdx)
        X(ovfIdx) = fixMIN;
    end     
    Y = X;
end